import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the PicDetailPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/pic-detail/pic-detail.html',
})
export class PicDetailPage {

	private data:any;	

	private width:any;
	private height:any;
	private timeout:any;
	private pinchW:any;
	private pinchH:any;

  constructor(private navCtrl: NavController, params: NavParams) {

  		this.data = params.get("data");

  }

}
