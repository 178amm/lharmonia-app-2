import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the NewDetailPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/new-detail/new-detail.html',
})
export class NewDetailPage {

  constructor(private navCtrl: NavController) {

  }

}
