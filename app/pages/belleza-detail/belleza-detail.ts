import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the BellezaDetailPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/belleza-detail/belleza-detail.html',
})
export class BellezaDetailPage {

	private belleza:any;
	private title:String;

  constructor(private navCtrl: NavController, params: NavParams) {

  	if (params.get("belleza")){
  		this.belleza = params.get("belleza");
  		this.title = 'Belleza'; 
  	}
  	else{
  		this.belleza = params.get("dama");
  		this.title = 'Dama';
  	}  	  
  }

}
