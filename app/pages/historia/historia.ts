import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import {TimerWrapper} from '@angular/core/src/facade/async';

/*
  Generated class for the HistoriaPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

declare var google: any;

@Component({
  templateUrl: 'build/pages/historia/historia.html',
})
export class HistoriaPage {

	private map: any;
	private loading:boolean;

  constructor(private navCtrl: NavController, private platform: Platform) {

  	//Init
  	this.loading = true;
  	this.platform = platform;  	
  	this.initializeMap();
  }

  	initializeMap() {	
		TimerWrapper.setTimeout(() => {  
			console.log('Map loaded!');
			this.loading=false;

		    let minZoomLevel = 17;
		    let position = new google.maps.LatLng(38.329717, -0.513922);

		    //Map init.
		    this.map = new google.maps.Map(document.getElementById('map_canvas_h'), {
		        zoom: minZoomLevel,
		        center: position,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		    });

		    //Marker
			let marker = new google.maps.Marker({
			    position: position,
			    map: this.map,
			    //title: 'Hello World!'
			  });
			marker.setMap(this.map);
		
	}, 1500); 
}
}