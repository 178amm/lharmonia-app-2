import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import {PicDetailPage} from '../pic-detail/pic-detail';

/*
  Generated class for the NuestraHogueraPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/nuestra-hoguera/nuestra-hoguera.html',
})
export class NuestraHogueraPage {

	private data: any[] = [
		{
			"order" : 1,
			"file" : '01-CENA DECIMO ANIVERSARIO 2015.jpg',
			"title" : 'Cena décimo aniversario',
			"year" : 2015,
		},
		{
			"order" : 2,
			"file" : '02-BELLEZAS EN IFA 2015.jpg',
			"title" : 'Bellezas en IFA',
			"year" : 2015,
		},
		{
			"order" : 3,
			"file" : '03-FIESTA DE LOS FOGUERERS INFANTILES 2015.jpg',
			"title" : 'Fiesta de los foguerers infantiles',
			"year" : 2015,
		},
		{
			"order" : 4,
			"file" : '04-ENSAYO CERTAMEN ARTISTICO 2015.jpg',
			"title" : 'Ensayo certámen artístico',
			"year" : 2015,
		},
		{
			"order" : 5,
			"file" : '05-CERTAMEN ARTISTICO 2015.jpg',
			"title" : 'Certámen artístico',
			"year" : 2015,
		},
		{
			"order" : 6,
			"file" : '06-HALLOWEEN 2015.jpg',
			"title" : 'Halloween',
			"year" : 2015,
		},
		{
			"order" : 7,
			"file" : '07-FOGUERES EN NADAL 2015.jpg',
			"title" : 'Fogueres en Nadal',
			"year" : 2015,
		},
		{
			"order" : 9,
			"file" : '09-PRESENTACION DE BELLEZAS Y DAMAS DEL SECTOR SUR 2015.jpg',
			"title" : 'Presentacion de bellezas y damas',
			"year" : 2015,
		},
		{
			"order" : 10,
			"file" : '10-PRESENTACION FALLA HERMANA 2016.jpg',
			"title" : 'Presentación Falla hermana',
			"year" : 2016,
		},
		{
			"order" : 11,
			"file" : '11-EN NUESTRA FALLA 2016.jpg',
			"title" : 'En nuestra falla',
			"year" : 2016,
		},
		{
			"order" : 12,
			"file" : '12-SANTA FAZ 2016.jpg',
			"title" : 'Santa Faz',
			"year" : 2016,
		},
		{
			"order" : 13,
			"file" : '13-PRESENTACIONES 2016.jpg',
			"title" : 'Presentaciones',
			"year" : 2016,
		},
		{
			"order" : 14,
			"file" : '14-CENA DE LA PRESENTACION 2016.jpg',
			"title" : 'Cena de la presentación',
			"year" : 2016,
		},
		{
			"order" : 15,
			"file" : '15-NUESTRA PRESENTACION 2016.jpg',
			"title" : 'Nuestra presentación',
			"year" : 2016,
		},
		{
			"order" : 16,
			"file" : '16-PASARELA INFANTIL 2016.jpg',
			"title" : 'Pasarela infantil',
			"year" : 2016,
		},
		{
			"order" : 17,
			"file" : '17-PASARELA ADULTA 2016.jpg',
			"title" : 'Pasarela adulta',
			"year" : 2016,
		},
		{
			"order" : 18,
			"file" : '18-ALMUERZO 2016.jpg',
			"title" : 'Almuerzo',
			"year" : 2016,
		},
		{
			"order" : 19,
			"file" : '19-FIESTA SECTOR SUR 2016.jpg',
			"title" : 'Fiesta sector sur',
			"year" : 2016,
		},
		{
			"order" : 20,
			"file" : '20-DESFILE DEL PREGON 2016.jpg',
			"title" : 'Desfile del pregón',
			"year" : 2016,
		},
		{
			"order" : 21,
			"file" : '21-DESFILE DEL NINOT 2016.jpg',
			"title" : 'Desfile del ninot',
			"year" : 2016,
		},
		{
			"order" : 22,
			"file" : '22-VISITA EXPOSICION NINOT 2016.jpg',
			"title" : 'Visita exposicion ninot',
			"year" : 2016,
		},
		{
			"order" : 23,
			"file" : '23-ENTREGA CORBATIN A LA COMISION INFANTIL 2016.jpg',
			"title" : 'Entrega corbatin',
			"year" : 2016,
		},
		{
			"order" : 24,
			"file" : '24-RECEPCION ALCALDE 2016.jpg',
			"title" : 'Recepcion alcalde',
			"year" : 2016,
		},
		{
			"order" : 25,
			"file" : '25-PLANTA 2016.jpg',
			"title" : 'Plantà',
			"year" : 2016,
		},
		{
			"order" : 26,
			"file" : '26-MANANA DE TURIBUS EN HOGUERAS 2016.jpg',
			"title" : 'Mañana de tribus',
			"year" : 2016,
		},
		{
			"order" : 27,
			"file" : '27-DEGUSTACION DE ANIS TENIS HOGUERAS 2016.jpg',
			"title" : 'Degustación anís Tenis',
			"year" : 2016,
		},
		{
			"order" : 28,
			"file" : '28-NOCHE DE HOGUERAS 2016.jpg',
			"title" : 'Noche de hogueras',
			"year" : 2016,
		},
		{
			"order" : 29,
			"file" : '29-PASACALLES EN NUESTRO DISTRITO 2016.jpg',
			"title" : 'Pascalles en nuestro distrito',
			"year" : 2016,
		},
		{
			"order" : 30,
			"file" : '30-OFRENDA DE FLORES A NUESTRO PATRON SAN GABRIEL 2016.jpg',
			"title" : 'Ofrenda de flores',
			"year" : 2016,
		},
		{
			"order" : 31,
			"file" : '31-CREMA 2016.jpg',
			"title" : 'Cremà',
			"year" : 2016,
		},
		{
			"order" : 32,
			"file" : '32-OFRENDA EN LAS FIESTAS DE NUESTRO BARRIO 2016.jpg',
			"title" : 'Ofrenda en nuestro barrio',
			"year" : 2016,
		},
	]

  constructor(private navCtrl: NavController) {

  }

  //Funcs
  navigate(data:any){
  	let parsed = {
  		header: 'Foto',
  		pic: 'img/nuestra-hoguera/' + data.file,
  		title: data.title,
  		subtitle: 'Año ' + data.year,
  	}

  	console.log(data);

  	this.navCtrl.push(PicDetailPage, {data: parsed});
  }
}
