import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import {PicDetailPage} from '../pic-detail/pic-detail';

/*
  Generated class for the MonumentoPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/monumento/monumento.html',
})
export class MonumentoPage {

	private adulto:any={
		pic: 'img/monumento/monumento-adulto-2016.png',
		title: 'Monumento Adulto',
		year: 2016
	}

	private infantil:any={
		pic: 'img/monumento/monumento-infantil-2016.png',
		title: 'Monumento Infantil',
		year: 2016
	}

  constructor(private navCtrl: NavController) {

  }

  //Funcs
  navigate(data:any){
  	
  	let parsed = {
  		header: data.title,
  		pic: data.pic,
  		title: data.title,
  		subtitle: 'Moumento de Foguera L\'harmonia' + data.year,
  	}

  	console.log(data);

  	this.navCtrl.push(PicDetailPage, {data: parsed});
  }

}
