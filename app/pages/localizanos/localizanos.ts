import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import {TimerWrapper} from '@angular/core/src/facade/async';

/*
  Generated class for the LocalizanosPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

declare var google: any;

@Component({
  templateUrl: 'build/pages/localizanos/localizanos.html',
})
export class LocalizanosPage {

	private map: any;
	private lat: number;
	private lng: number;

  constructor(private navCtrl: NavController, private platform: Platform) {

  	//Init
  	this.platform = platform;  	
  	this.lat = 38.329717;
  	this.lng = -0.513922;

  	this.initializeMap();
  }

  	initializeMap() {

		TimerWrapper.setTimeout(() => {  
			console.log('Timer out, loading map...!');

		    let minZoomLevel = 17;
		    let position = new google.maps.LatLng(this.lat, this.lng);

		    //Map init.
		    this.map = new google.maps.Map(document.getElementById('map_canvas'), {
		        zoom: minZoomLevel,
		        center: position,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		    });

		    //Marker
			let marker = new google.maps.Marker({
			    position: position,
			    map: this.map,
			    //title: 'Hello World!'
			  });
			marker.setMap(this.map);
		}, 1500); 
	}

	center(){
		this.map.setCenter(new google.maps.LatLng(this.lat, this.lng));
	}  

}
