import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import {BellezaDetailPage} from '../belleza-detail/belleza-detail';

/*
  Generated class for the BellezasPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/bellezas/bellezas.html',
})
export class BellezasPage {

  private bellezas: any[] = [
    {
      name: 'Paloma Llopis Tortosa',
      class: 'Belleza Adulta',
      year: 2016,
      pic: 'img/bellezas/belleza-adulto-1-2016'
    }
  ]

  private damas: any[] = [
    {
      name: 'Marta Campos Marquez',
      class: 'Dama Infantil',
      year: 2016,
      pic: 'img/bellezas/dama-infantil-1-2016'
    },
    {
      name: 'Lucia Pastor Gisbert',
      class: 'Dama Infantil',
      year: 2016,
      pic: 'img/bellezas/dama-infantil-2-2016'
    }
  ]

  constructor(private navCtrl: NavController) {

  }

  //Funcs
  navigate(belleza:any){
  	this.navCtrl.push(BellezaDetailPage, belleza);
  }

}
