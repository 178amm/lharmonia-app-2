import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import {NewDetailPage} from '../new-detail/new-detail';

/*
  Generated class for the DashboardPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/dashboard/dashboard.html',
})
export class DashboardPage {

  private publi: Number[];
  private popup: string = "";

  constructor(private navCtrl: NavController) {
    this.publi = new Array(6);
  }  

  open(i){
    this.popup = 'img/publi/'+i+'.jpg';
  }
}
