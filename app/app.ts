//Components
import {Component, ViewChild} from '@angular/core';
import {ionicBootstrap, Platform, MenuController, Nav} from 'ionic-angular';
import {StatusBar} from 'ionic-native';
//Pages
import {DashboardPage} from './pages/dashboard/dashboard';
import {HistoriaPage} from './pages/historia/historia';
import {MonumentoPage} from './pages/monumento/monumento';
import {BellezasPage} from './pages/bellezas/bellezas';
import {LocalizanosPage} from './pages/localizanos/localizanos';
import {NuestraHogueraPage} from './pages/nuestra-hoguera/nuestra-hoguera';
import {AgendaPage} from './pages/agenda/agenda';

import {NewDetailPage} from './pages/new-detail/new-detail';
import {BellezaDetailPage} from './pages/belleza-detail/belleza-detail';
 


@Component({
  // template: '<ion-nav [root]="rootPage"></ion-nav>'
  templateUrl: 'build/app.html'
})
export class Lharmonia {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  pages: Array<{title: string, component: any, icon: String}>;

  constructor(  
    public platform: Platform,
    public menu: MenuController) {

    //Root page
    this.rootPage = DashboardPage;
    // set our app's pages
    this.pages = [
      { title: 'Inicio', component: DashboardPage, icon: 'home' },
      { title: 'Nuestra historia', component: HistoriaPage, icon: 'book' },
      { title: 'El monumento', component: MonumentoPage, icon: 'bonfire'  },
      { title: 'Bellezas y damas', component: BellezasPage, icon: 'flame'  },
      { title: 'Localizanos', component: LocalizanosPage, icon: 'pin'  },
      { title: 'Un año en nuestra hoguera', component: NuestraHogueraPage, icon: 'home' },
      { title: 'Agenda de actos', component: AgendaPage, icon: 'calendar' },

      // { title: 'My First List', component: ListPage }
    ];

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }

    openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}

ionicBootstrap(Lharmonia);
