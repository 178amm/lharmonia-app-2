# L'harmonia App 2
App de la Foguera l'Harmonia para Android e iOS

## [Unreleased]

### Changed

### Fixed

## [1.0.1] - 2016-09-15
- Arreglado bug con gmaps
- Agenda de actos actualizada

## [1.0.0] - 2016-09-15
### Added

- Implementar banners publicidad
- Algunos fixes graficos
- Implementar el como llegar y el centrado en localizanos

Vistas completadas:
- Nuestra historia vista 
- El monumento
- Bellezas y damas
- Donde localizarnos
- Un año en nuestra hoguera
- Agenda de actos


## [0.0.0] - 2015-03-30
### Added
- Project initialization

[0.2]: https://bitbucket.org/dadrimon2-admin/postsingson-mobile-app/branches/compare/v0.2.0%0Dv0.1.0
[0.1]: https://bitbucket.org/dadrimon2-admin/postsingson-mobile-app/branches/compare/v0.1.0%0Dmaster#diff